# TICK-TOCK

For source code pls checkout to develop.

Frontend:
*   RectJS

    > React routing 
    
    > React redux
    
    > React components
*   Ant Design (React UI library)

    > Layout
    
    > Grid
    
    > Button
    
    > Form
    
    > Others...
    
Backend:

*   Laravel(version 5.8)

    >


## Domain
[http://tick-tock-proj.herokuapp.com/](http://tick-tock-proj.herokuapp.com/)


### Prístupy
Type | Username | Password
--- | --- | ---
User | user@user.sk | Abcd1234

### Databáza
Mysql 5.7

Type | Database | Username | Password | Host | Port
--- | --- | --- | --- | --- | ---
Public | sql7289900 | sql7289900 | bRsyiqPUIq | sql7.freemysqlhosting.net | 3306




